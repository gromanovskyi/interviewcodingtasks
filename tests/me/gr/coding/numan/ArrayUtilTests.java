package me.gr.coding.numan;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ArrayUtilTests {

    @Test
    public void testPrintEqualValueSidesNumbers_cornerCases() {
        assertEquals(-1, ArrayUtils.printEqualValueSidesNumbersN2(null));
        assertEquals(-1, ArrayUtils.printEqualValueSidesNumbersN2(new int[0]));
        assertEquals(-1, ArrayUtils.printEqualValueSidesNumbersN2(new int[]{1}));
        assertEquals(0, ArrayUtils.printEqualValueSidesNumbersN2(new int[]{1, 1}));
    }

    @Test
    public void testPrintEqualValueSidesNumbers_normalCases() {
        assertEquals(2, ArrayUtils.printEqualValueSidesNumbersN2(new int[]{1, 1, 0, 1, -11, 1, 11}));
        assertEquals(1, ArrayUtils.printEqualValueSidesNumbersN2(new int[]{1, 0, -11, 1, 11}));
        assertEquals(1, ArrayUtils.printEqualValueSidesNumbersN2(new int[]{1, 1, -11, 2, 11}));
        assertEquals(-1, ArrayUtils.printEqualValueSidesNumbersN2(new int[]{1, 1, 1, 1, 1}));
        assertEquals(2, ArrayUtils.printEqualValueSidesNumbersN2(new int[]{1, 1, 1, 1, 1, 1}));
        assertEquals(2, ArrayUtils.printEqualValueSidesNumbersN2(new int[]{1, 2, 3, 3, 2, 1}));
    }

    @Test
    public void testPrintEqualValueSidesNumbers2N_cornerCases() {
        assertEquals(-1, ArrayUtils.printEqualValueSidesNumbers2N(null));
        assertEquals(-1, ArrayUtils.printEqualValueSidesNumbers2N(new int[0]));
        assertEquals(-1, ArrayUtils.printEqualValueSidesNumbers2N(new int[]{1}));
        assertEquals(0, ArrayUtils.printEqualValueSidesNumbers2N(new int[]{1, 1}));
    }

    @Test
    public void testPrintEqualValueSidesNumbers2N_normalCases() {
        assertEquals(2, ArrayUtils.printEqualValueSidesNumbers2N(new int[]{1, 1, 0, 1, -11, 1, 11}));
        assertEquals(1, ArrayUtils.printEqualValueSidesNumbers2N(new int[]{1, 0, -11, 1, 11}));
        assertEquals(1, ArrayUtils.printEqualValueSidesNumbers2N(new int[]{1, 1, -11, 2, 11}));
        assertEquals(-1, ArrayUtils.printEqualValueSidesNumbers2N(new int[]{1, 1, 1, 1, 1}));
        assertEquals(2, ArrayUtils.printEqualValueSidesNumbers2N(new int[]{1, 1, 1, 1, 1, 1}));
        assertEquals(2, ArrayUtils.printEqualValueSidesNumbers2N(new int[]{1, 2, 3, 3, 2, 1}));
    }
}