package me.gr.coding.numan;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SystemOfEquationsTests {

    @Test
    public void testCalculateFunctionOf() {
        assertEquals(0, SystemOfEquations.calculateFunctionOf(0));
        assertEquals(1, SystemOfEquations.calculateFunctionOf(2));
        assertEquals(5, SystemOfEquations.calculateFunctionOf(3));
        assertEquals(8, SystemOfEquations.calculateFunctionOf(5));
    }
}