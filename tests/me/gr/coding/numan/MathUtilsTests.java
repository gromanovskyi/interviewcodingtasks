package me.gr.coding.numan;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MathUtilsTests {

    @Test
    public void testPower() {
        assertEquals(Math.pow(2, 0), MathUtils.power(2, 0), 0.0);
        assertEquals(Math.pow(2, 1), MathUtils.power(2, 1), 0.0);
        assertEquals(Math.pow(2, 2), MathUtils.power(2, 2), 0.0);
        assertEquals(Math.pow(2, 3), MathUtils.power(2, 3), 0.0);
        assertEquals(Math.pow(2, 5), MathUtils.power(2, 5), 0.0);
        assertEquals(Math.pow(2, -1), MathUtils.power(2, -1), 0.0);
        assertEquals(Math.pow(2, -2), MathUtils.power(2, -2), 0.0);
    }

    @Test
    public void textXInPowerOfY() {
        assertEquals(Math.pow(2, 0), MathUtils.xInPowerOfY(2, 0), 0.0);
        assertEquals(Math.pow(2, 1), MathUtils.xInPowerOfY(2, 1), 0.0);
        assertEquals(Math.pow(2, 2), MathUtils.xInPowerOfY(2, 2), 0.0);
        assertEquals(Math.pow(2, 3), MathUtils.xInPowerOfY(2, 3), 0.0);
        assertEquals(Math.pow(2, 5), MathUtils.xInPowerOfY(2, 5), 0.0);
        assertEquals(Math.pow(2, -1), MathUtils.xInPowerOfY(2, -1), 0.0);
        assertEquals(Math.pow(2, -2), MathUtils.xInPowerOfY(2, -2), 0.0);
    }

    @Test
    public void testIsPowerOf2() {
        // positive
        assertTrue(MathUtils.isPowerOf2(2));
        assertTrue(MathUtils.isPowerOf2(4));
        assertTrue(MathUtils.isPowerOf2(8));
        assertTrue(MathUtils.isPowerOf2(16));
        assertTrue(MathUtils.isPowerOf2(32));
        // negative
        assertFalse(MathUtils.isPowerOf2(3));
        assertFalse(MathUtils.isPowerOf2(6));
        assertFalse(MathUtils.isPowerOf2(12));
        assertFalse(MathUtils.isPowerOf2(13));
    }

    @Test
    public void testMultiplyBy7() {
        assertEquals(0, MathUtils.multiplyBy7(0));
        assertEquals(7, MathUtils.multiplyBy7(1));
        assertEquals(2 * 7, MathUtils.multiplyBy7(2));
        assertEquals(21, MathUtils.multiplyBy7(3));
        assertEquals(4 * 7, MathUtils.multiplyBy7(4));
        assertEquals(1024 * 7, MathUtils.multiplyBy7(1024));
        assertEquals(-1000 * 7, MathUtils.multiplyBy7(-1000));
        assertEquals(49 * 7, MathUtils.multiplyBy7(49));
    }
}