package me.gr.coding.numan;

public class SystemOfEquations {

    /**
     * Given f(n) = n⁄2, when n is even and f(n)=f(3n+1), when n is odd. Write recursive function to
     * compute f(n).
     */
    public static int calculateFunctionOf(int n) {
        if (n % 2 == 0) {
            return n / 2;
        } else {
            return calculateFunctionOf(3 * n + 1);
        }
    }
}
