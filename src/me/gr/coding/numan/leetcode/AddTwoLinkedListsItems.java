package me.gr.coding.numan.leetcode;

/**
 * TODO: implement solution base on a loop
 */
public class AddTwoLinkedListsItems {

    /**
     *
     Input:Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
     (2 -> 4 Output: 7 -> 0 -> 8
     Explanation: 342 + 465 = 807.
     * @param args
     */
    public static void main(String[] args) {
        ListNode node = addTwoNumbers(getList1(), getList2());

        while (node != null) {
            System.out.println(node.val);
            node = node.next;
        }
    }

    private static ListNode getList1() {
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(4);
        ListNode node3 = new ListNode(3);

        node1.next = node2;
        node2.next = node3;
        return node1;
    }

    private static ListNode getList2() {
        ListNode node1 = new ListNode(5);
        ListNode node2 = new ListNode(6);
        ListNode node3 = new ListNode(4);

        node1.next = node2;
        node2.next = node3;
        return node1;
    }

    /**
     * Implementation based on the tail recursion.
     */
    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        return addTwoNodes(l1, l2, 0);
    }

    private static ListNode addTwoNodes(ListNode l1, ListNode l2, int carryOver) {
        if (l1 == null || l2 == null) {
            if (l1 == null && l2 == null) {
                if (carryOver > 0) {
                    return new ListNode(carryOver);
                }
                return null;
            }
            ListNode nodeToCopy = l1 == null ? l2 : l1;
            ListNode node = new ListNode((nodeToCopy.val + carryOver) % 10);
            node.next = addTwoNodes(nodeToCopy.next, null, (nodeToCopy.val + carryOver) / 10);
            return node;
        } else {
            ListNode node = new ListNode((l1.val + l2.val + carryOver) % 10);
            node.next = addTwoNodes(l1.next, l2.next, (l1.val + l2.val + carryOver) / 10);
            return node;
        }
    }

    private static class ListNode {
      int val;
      ListNode next;
      ListNode(int x) { val = x; }
   }
}
