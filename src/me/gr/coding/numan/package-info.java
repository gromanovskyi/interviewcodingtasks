package me.gr.coding.numan;

/**
 * <h1>Numbers manipulations</h1>
 * <p>This group contains of mathematical operations of numbers, calculations, bit manipulations.
 * Candidate  will be able to solve the tasks of reversing or signing bits, write by yourself
 * the functions of Math. class and have knowledge of sequenced numbers.</p>
 * <ol>
 * <li>Write a function to calculate x in the power of y.</li>
 * <li>Write a program to display numbers having sum of left side numbers equal to right side numbers.
 * E.g. {1,0,-11,1,12}=>0 {Left side number 1+0=1, Right side number -11+1+11=1}.</li></li>
 * </ol>
 *
 * Self-study:
 * <ol>
 * <li>Given f(n) = n⁄2, when n is even and f(n)=f(3n+1), when n is odd. Write recursive function to compute f(n).</li>
 * <li>Given two positive floating point numbers (x,y), calculate x/y to within a specified epsilon without using in-built functions.</li>
 * <li>Give a one-line expression to test whether a number is a power of 2.</li>
 * <li>Give a fast way to multiply a number by 7.</li>
 * <li>Reverse the bits of an unsigned integer.</li>
 * <li>Write a program that prints the numbers from 1 to 100. But for multiples of three prints “Fizz” instead of the number and for the multiples of five prints “Buzz”. For numbers, which are, multiples of both three and five prints “FizzBuzz”.</li>
 * <li>Find a unique number in 883366221885522.</li>
 * </ol>
 */